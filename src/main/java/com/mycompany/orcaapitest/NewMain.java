package com.mycompany.orcaapitest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author tungn
 */
public class NewMain {

    private static final String URL_HTTP = "http://52.199.136.66:8000";
    private static final String userDB = "ormaster";
    private static final String passDB = "ormaster";

    private static final String SYSTEM01LST = "/api01rv2/system01lstv2";
    private static final String PATIENT01LST = "/api01rv2/patientlst2v2";
    private static final String PATIENTGET = "/api01rv2/patientgetv2";
    private static final String GETDISEASE = "/api01rv2/diseasegetv2";
    private static final String MANAGEUSER = "/orca101/manageusersv2";
    private static final String PATIENTMOD = "/orca12/patientmodv2";
    private static final String HSPTINFOMOD = "/orca31/hsptinfmodv2";

    public static final String KIND_01 = "?class=01";
    public static final String KIND_02 = "?class=02";
    public static final String KIND_03 = "?class=03";
    public static final String KIND_04 = "?class=04";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerConfigurationException, TransformerException, CloneNotSupportedException {
        HttpResponse postRequest;

        switch (0) {
            case 1:
                //Manage Users
                postRequest = manageusersv2("01");
                break;
            case 2:
                //Get List Patient
                postRequest = patient01lst("00002", KIND_01);
                break;
            case 3:
                //Get Patient by id
                postRequest = patientgetv2("00002");
                break;
            case 4:
                //Get DISEASE by Patient id
                postRequest = diseaseget("00002", "2017-06", KIND_01);
                break;
            case 5:
                //Register Patient
                postRequest = patientmodv2(KIND_01);
                break;
            case 6:
                //Register hospital
                postRequest = hsptinfmodv2("00016", "2017-06-15", "01");
                break;
            default:
                //Get System Info v1
                postRequest = system01lst("2017-06-15", KIND_04);
                break;
        }

        HttpEntity entity = postRequest.getEntity();

        if (entity.getContentLength() == 0) {
            System.out.println(postRequest.getEntity().getContentLength());
        } else {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(entity.getContent());

            TransformerFactory tfactory = TransformerFactory.newInstance();
            Transformer xform = tfactory.newTransformer();

            File myOutput = new File("C:\\Users\\tungn\\Desktop\\myOutput.xml");
            if (myOutput.exists()) {
                myOutput.delete();
            }
            xform.transform(new DOMSource(doc), new StreamResult(myOutput));
            StringWriter writer = new StringWriter();
            xform.transform(new DOMSource(doc), new StreamResult(writer));
            System.out.println(writer.toString());
        }
    }

    private static HttpResponse system01lst(String date, String kind) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<system01_managereq type=\"record\">");
        sbParam.append("<Base_Date type=\"string\">");
        sbParam.append(date);
        sbParam.append("</Base_Date>");
        sbParam.append("</system01_managereq>");
        sbParam.append("</data>");

        return postRequest(SYSTEM01LST + kind, sbParam.toString());
    }

    private static HttpResponse manageusersv2(String requestNumber) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<manageusersreq type=\"record\">");
        sbParam.append("<Request_Number type=\"string\">");
        sbParam.append(requestNumber);
        sbParam.append("</Base_Date>");
        sbParam.append("</manageusersreq>");
        sbParam.append("</data>");

        return postRequest(MANAGEUSER, sbParam.toString());
    }

    private static HttpResponse patient01lst(String id, String kind) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<patientlst2req type=\"record\">");
        sbParam.append("<Patient_ID_Information type=\"array\">");
        sbParam.append("<Patient_ID_Information_child type=\"record\">");
        sbParam.append("<Patient_ID type=\"string\">");
        sbParam.append(id);
        sbParam.append("</Patient_ID>");
        sbParam.append("</Patient_ID_Information_child>");
        sbParam.append("</Patient_ID_Information>");
        sbParam.append("</patientlst2req>");
        sbParam.append("</data>");

        return postRequest(PATIENT01LST + kind, sbParam.toString());
    }

    private static HttpResponse patientgetv2(String id) throws IOException {
        return postRequest(PATIENTGET + "?id=" + id, null);
    }

    private static HttpResponse diseaseget(String pid, String date, String kind) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<disease_inforeq type=\"record\">");
        sbParam.append("<Patient_ID type=\"string\">");
        sbParam.append(pid);
        sbParam.append("</Patient_ID>");
        sbParam.append("<Base_Date type=\"string\">");
        sbParam.append(date);
        sbParam.append("</Base_Date>");
        sbParam.append("</disease_inforeq>");
        sbParam.append("</data>");

        return postRequest(GETDISEASE + kind, sbParam.toString());
    }

    private static HttpResponse patientmodv2(String kind) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<patientmodreq type=\"record\">");
        sbParam.append("<Mod_Key type=\"string\">");
        sbParam.append("2");
        sbParam.append("</Mod_Key>");
        sbParam.append("<Patient_ID type=\"string\">");
        sbParam.append("*");
        sbParam.append("</Patient_ID>");
        sbParam.append("<WholeName type=\"string\">");
        sbParam.append("日医　太郎");
        sbParam.append("</WholeName>");
        sbParam.append("<WholeName_inKana type=\"string\">");
        sbParam.append("ニチイ　タロウ");
        sbParam.append("</WholeName_inKana>");
        sbParam.append("<BirthDate type=\"string\">");
        sbParam.append("1970-01-01");
        sbParam.append("</BirthDate>");
        sbParam.append("<Sex type=\"string\">");
        sbParam.append("1");
        sbParam.append("</Sex>");
        sbParam.append("<HouseHolder_WholeName type=\"string\">");
        sbParam.append("日医　太郎");
        sbParam.append("</HouseHolder_WholeName>");
        sbParam.append("<Relationship type=\"string\">");
        sbParam.append("本人");
        sbParam.append("</Relationship>");
        sbParam.append("<Occupation type=\"string\">");
        sbParam.append("会社員");
        sbParam.append("</Occupation>");
        sbParam.append("<CellularNumber type=\"string\">");
        sbParam.append("09011112222");
        sbParam.append("</CellularNumber>");
        sbParam.append("<FaxNumber type=\"string\">");
        sbParam.append("03-0011-2233");
        sbParam.append("</FaxNumber>");
        sbParam.append("<EmailAddress type=\"string\">");
        sbParam.append("test@tt.dot.jp");
        sbParam.append("</EmailAddress>");
        sbParam.append("<Home_Address_Information type=\"record\">");
        sbParam.append("<Address_ZipCode type=\"string\">");
        sbParam.append("1130021");
        sbParam.append("</Address_ZipCode>");
        sbParam.append("<WholeAddress1 type=\"string\">");
        sbParam.append("東京都文京区本駒込");
        sbParam.append("</WholeAddress1>");
        sbParam.append("<WholeAddress2 type=\"string\">");
        sbParam.append("６−１６−３");
        sbParam.append("</WholeAddress2>");
        sbParam.append("<PhoneNumber1 type=\"string\">");
        sbParam.append("03-3333-2222");
        sbParam.append("</PhoneNumber1>");
        sbParam.append("<PhoneNumber2 type=\"string\">");
        sbParam.append("03-3333-1133");
        sbParam.append("</PhoneNumber2>");
        sbParam.append("</Home_Address_Information>");
        sbParam.append("<WorkPlace_Information type=\"record\">");
        sbParam.append("<WholeName type=\"string\">");
        sbParam.append("てすと　株式会社");
        sbParam.append("</WholeName>");
        sbParam.append("<Address_ZipCode type=\"string\">");
        sbParam.append("1130022");
        sbParam.append("</Address_ZipCode>");
        sbParam.append("<WholeAddress1 type=\"string\">");
        sbParam.append("東京都文京区本駒込");
        sbParam.append("</WholeAddress1>");
        sbParam.append("<WholeAddress2 type=\"string\">");
        sbParam.append("５−１２−１１");
        sbParam.append("</WholeAddress2>");
        sbParam.append("<PhoneNumber type=\"string\">");
        sbParam.append("03-3333-2211");
        sbParam.append("</PhoneNumber>");
        sbParam.append("</WorkPlace_Information>");
        sbParam.append("<Contraindication1 type=\"string\">");
        sbParam.append("状態");
        sbParam.append("</Contraindication1>");
        sbParam.append("<Allergy1 type=\"string\">");
        sbParam.append("アレルギ");
        sbParam.append("</Allergy1>");
        sbParam.append("<Infection1 type=\"string\">");
        sbParam.append("感染症");
        sbParam.append("</Infection1>");
        sbParam.append("<Comment1 type=\"string\">");
        sbParam.append("コメント");
        sbParam.append("</Comment1>");
        sbParam.append("<HealthInsurance_Information type=\"record\">");
        sbParam.append("<InsuranceProvider_Class type=\"string\">");
        sbParam.append("060");
        sbParam.append("</InsuranceProvider_Class>");
        sbParam.append("<InsuranceProvider_Number type=\"string\">");
        sbParam.append("138057");
        sbParam.append("</InsuranceProvider_Number>");
        sbParam.append("<InsuranceProvider_WholeName type=\"string\">");
        sbParam.append("国保");
        sbParam.append("</InsuranceProvider_WholeName>");
        sbParam.append("<HealthInsuredPerson_Symbol type=\"string\">");
        sbParam.append("０１");
        sbParam.append("</HealthInsuredPerson_Symbol>");
        sbParam.append("<HealthInsuredPerson_Number type=\"string\">");
        sbParam.append("１２３４５６７");
        sbParam.append("</HealthInsuredPerson_Number>");
        sbParam.append("<RelationToInsuredPerson type=\"string\">");
        sbParam.append("1");
        sbParam.append("</RelationToInsuredPerson>");
        sbParam.append("<Certificate_StartDate type=\"string\">");
        sbParam.append("2010-05-01");
        sbParam.append("</Certificate_StartDate>");
        sbParam.append("<PublicInsurance_Information type=\"array\">");
        sbParam.append("<PublicInsurance_Information_child type=\"record\">");
        sbParam.append("<PublicInsurance_Class type=\"string\">");
        sbParam.append("010");
        sbParam.append("</PublicInsurance_Class>");
        sbParam.append("<PublicInsurance_Name type=\"string\">");
        sbParam.append("感３７の２");
        sbParam.append("</PublicInsurance_Name>");
        sbParam.append("<PublicInsurer_Number type=\"string\">");
        sbParam.append("10131142");
        sbParam.append("</PublicInsurer_Number>");
        sbParam.append("<PublicInsuredPerson_Number type=\"string\">");
        sbParam.append("1234566");
        sbParam.append("</PublicInsuredPerson_Number>");
        sbParam.append("<Certificate_IssuedDate type=\"string\">");
        sbParam.append("2010-05-01");
        sbParam.append("</Certificate_IssuedDate>");
        sbParam.append("</PublicInsurance_Information_child>");
        sbParam.append("</PublicInsurance_Information>");
        sbParam.append("</HealthInsurance_Information>");
        sbParam.append("</patientmodreq>");
        sbParam.append("</data>");

        return postRequest(PATIENTMOD + kind, sbParam.toString());
    }

    private static HttpResponse hsptinfmodv2(String id, String date, String requestOrder) throws IOException {
        StringBuilder sbParam = new StringBuilder();
        sbParam.append("<data>");
        sbParam.append("<private_objects type=\"record\">");
        sbParam.append("<Patient_ID type=\"string\">");
        sbParam.append(id);
        sbParam.append("</Patient_ID>");
        sbParam.append("<Request_Number type=\"string\">");
        sbParam.append(requestOrder);
        sbParam.append("</Request_Number>");
        sbParam.append("<Admission_Date type=\"string\">");
        sbParam.append(date);
        sbParam.append("</Admission_Date>");
        sbParam.append("<Ward_Number type=\"string\">");
        sbParam.append("01");
        sbParam.append("</Ward_Number>");
        sbParam.append("<Room_Number type=\"string\">");
        sbParam.append("101");
        sbParam.append("</Room_Number>");
        sbParam.append("<Room_Charge type=\"string\">");
        sbParam.append("1000");
        sbParam.append("</Room_Charge>");
        sbParam.append("<Department_Code type=\"string\">");
        sbParam.append("01");
        sbParam.append("</Department_Code>");
        sbParam.append("<Doctor_Code type=\"array\">");
        sbParam.append("<Doctor_Code_child type=\"string\">");
        sbParam.append("10001");
        sbParam.append("</Doctor_Code_child>");
        sbParam.append("</Doctor_Code>");
        sbParam.append("<HealthInsurance_Information type=\"record\">");
        sbParam.append("<InsuranceProvider_Class type=\"string\">");
        sbParam.append("060");
        sbParam.append("</InsuranceProvider_Class>");
        sbParam.append("</HealthInsurance_Information>");
        sbParam.append("<Hospital_Charge type=\"string\">");
        sbParam.append("190799410");
        sbParam.append("</Hospital_Charge>");
        sbParam.append("<Recurring_Billing type=\"string\">");
        sbParam.append("1");
        sbParam.append("</Recurring_Billing>");
        sbParam.append("</private_objects>");
        sbParam.append("</data>");

        return postRequest(HSPTINFOMOD, sbParam.toString());
    }

    public static HttpResponse postRequest(String Api, String messageBody) throws IOException {
        String httpUrl = URL_HTTP + Api;
        System.out.println(httpUrl);
        HttpPost request = new HttpPost(httpUrl);
        return makeRequest(request, messageBody);
    }

    private static HttpResponse makeRequest(HttpEntityEnclosingRequestBase request, String messageBody) throws IOException {
        System.out.println(messageBody);
        System.out.println(userDB + ":" + passDB);
        byte[] encoded = Base64.encodeBase64((userDB + ":" + passDB).getBytes());
        request.addHeader("Authorization", "Basic " + new String(encoded));
        request.addHeader("Content-Type", "application/xml;charset=UTF-8");
        if (messageBody != null) {
            StringEntity requestBody = new StringEntity(messageBody, "UTF-8");
            request.setEntity(requestBody);
        }
        HttpClient client = HttpClientBuilder.create().build();
        return client.execute(request);
    }

}
